var express = require('express');
var router = express.Router();
var request = require('request');
var btoa = require('btoa');
var config = require('../config/config.json');

router.post('/', function(req, res, next) {
  console.log(JSON.stringify(config));
    var options = {
        method: 'POST',
        url: config.apiurl,
        headers: {
            authorization: 'Basic ' + btoa(config.username + ':' + config.password),
            'content-type': 'multipart/form-data; boundary=---011000010111000001101001'
        },
        formData: {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email_id: req.body.email_id,
            date: req.body.date,
            time: req.body.time,
            age: req.body.age
        }
    };

    request(options, function(error, response, body) {
        if (error) throw new Error(error);

        console.log(body);
        res.send(body);

    });

});

module.exports = router;
