# README #

### Prerequisite ###

* You need to install Node.js on your machine.


### How to Run this project ###

* Clone/Download this project
* cd <project_name>
* run 'npm install'
* run 'npm start'

### Configuration ###

* Open 'config/config.json'
* Add API url, username, password

### Get unique Id ###

* Run the server using 'npm start'
* Open 'web/index.html' in browser and click that button on webpage.
